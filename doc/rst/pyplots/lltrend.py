#import libraries
import numpy as np
from pyssm.ssm import Filter, SimSmoother
from pymcmc.regtools import CondScaleSampler
from pymcmc.mcmc import CFsampler, MCMC
import matplotlib.pyplot as plt

#Set seed for random number generator
np.random.seed(12345)

def simdata():
    #Define system dimensions
    nobs = 1000; nseries = 1; nstate = 2; rstate = 2

    #Define system matricies
    zt = np.array(((1., 0.)))
    rt = 1.; ht = 0.6 ** 2
    tt = np.array(((1., 1.), (0., 1.)))
    gt = np.eye(2); p1 = np.eye(2) * 0.1 ** 2
    qt = np.diag((0.4, 0.01))
    a1 = np.array((5., 0.1))

    #Define filter class
    timevar = False #No timevarying system matrices
    filt = Filter(nobs, nseries, nstate, rstate, timevar)


    #Initialise system matrices
    filt.initialise_system(a1, p1, zt, ht, tt, gt, qt, rt)
    filt.simssm() # Simulate data

    yvec = filt.get_ymat().T.flatten() #Get simulated data
    state = filt.get_state() #Get simulated state

    return yvec, state

def sim_ht(store):
    "Function to simulate ht"

    system = store['simsm'].get_system()
    res = store['simsm'].get_meas_residual()
    ht = store['scale_sampler'].sample(res, 1) ** 2 # Note 1 implies transpose is used
    system.update_ht(ht)
    return ht

def sim_qt(store):
    "Function to simulate qt"

    system = store['simsm'].get_system()
    res = store['simsm'].get_state_residual()

    #Sample the diagonal of qt
    dqt = store['scale_sampler'].sample(res, 1) ** 2
    system.update_qt(np.diag(dqt))
    return dqt

def simstate(store):
    "Function simulates state"

    return store['simsm'].sim_smoother()

def main():
    "Main function."

    #Simulate data
    yvec, simulated_state = simdata()

    #define data dictionary that all function called
    #from MCMC engine will have access to.
    data = {'yvec': yvec}

    #Define simulation smoother class
    data['simsm'] = SimSmoother(yvec, 2, 2, False,
                   properties = {'gt': 'eye'}) 

    #Initial values for system matrices
    zt = np.array(((1., 0.)))
    rt = 1.; ht = 1.
    tt = np.array([(1., 1.), (0., 1.)])
    gt = np.eye(2)
    qt = 0.3 ** 2 * np.eye(2)
    a1 = np.zeros(2)
    p1 = np.eye(2) * 10.

    data['simsm'].initialise_system(a1, p1, zt, ht, tt, gt, qt, rt)

    #Define class for sampling scale with nu = 10 and S = 0.1
    data['scale_sampler'] = CondScaleSampler(prior = ['inverted_gamma', 10, 0.01])

    #Define block to sample state (Note first in Gibbs scheme so don't need
    #clever initialisation)
    sample_state = CFsampler(simstate, np.zeros((2, yvec.shape[0])), 'state', store = 'none')

    #Define block to sample ht
    sample_ht = CFsampler(sim_ht, 1., 'ht')

    #Define block to sample qt
    sample_qt = CFsampler(sim_qt, np.ones(2) * 0.3, 'qt')

    #Define MCMC scheme(8000 iterations, 2000 discarded)
    mcmc = MCMC(8000, 2000, data, [sample_state, sample_ht, sample_qt],
                runtime_output = True)
    mcmc.sampler()
    mcmc.output(parameters = ['ht', 'qt'])

    #Get posterior mean and variance
    mstate, vstate = mcmc.get_mean_var('state')

    #plot actual trend versus estimated trend
    plt.plot(simulated_state[0])
    plt.plot(mstate[0])
    plt.title("Estimated vs actual trend")
    plt.show()


    




#Run main function
main()


    




