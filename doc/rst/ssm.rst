Linear Gaussian State Space Model
=================================

For a :math:`(p\times 1)` vector of observations, :math:`\mathbf{y}_t`, the observation equation
of a linear Gaussian state space model (SSM) is given by,

.. math:: \mathbf{y}_t=\mathbf{X}_t\boldsymbol{\beta}_m+\mathbf{Z}_t\mathbf{x}_t+
    \mathbf{R}_t\boldsymbol{\varepsilon}_t;
    \,\,\, \boldsymbol{\varepsilon}_t\sim N\left(\mathbf{0}, \mathbf{H}_t\right),
    :label: Observation_SSM    

where :math:`\mathbf{X}_t` is a :math:`(p\times k_m)` matrix of regressors,
:math:`\boldsymbol{\beta}` is a :math:`(k_m\times 1)` vector of regression coefficients,
:math:`\mathbf{Z}_t` is a :math:`(p\times m)` is a system matrix, :math:`\mathbf{x}_t` 
is a :math:`(m\times 1)` state vector, :math:`\mathbf{R}_t` is a :math:`(p\times p)`
system matrix, :math:`\boldsymbol{\varepsilon}_t` is a :math:`(p\times 1)` vector of 
normally distributed random variables, with a mean vector :math:`\mathbf{0}` and a 
:math:`(p\times p)` covariance matrix :math:`\mathbf{H}_t.` The state equation is generated
as follows:

.. math:: \mathbf{x}_{t+1}=\mathbf{W}_t\boldsymbol{\beta}_s +
    \mathbf{T}_t\mathbf{x}_t+\mathbf{G}_t \boldsymbol{\eta}_t;\,\,\, \boldsymbol{\eta}_t\sim N\left(\mathbf{0}, 
    \mathbf{Q}_t\right),
    :label: State_SSM

where :math:`\mathbf{W}_t`  is an :math:`(m\times k_s)` matrix of regressors, :math:`\boldsymbol{\beta}_s`
is an :math:`(k_s\times 1)` vector of regression coefficients, :math:`\mathbf{T}_t` is an 
:math:`(m\times m)` transition matrix, :math:`\mathbf{G}_t` is an :math:`(m\times r)` system matrix,
:math:`\boldsymbol{\eta}_t` is an :math:`(r\times 1)` vector of normally distributed random variables, with a
mean vector :math:`\mathbf{0}` and a covariance matrix :math:`\mathbf{Q}_t`. The initial state, :math:`\mathbf{x}_1`,
is given by

.. math:: \mathbf{x}_1\sim N\left(\mathbf{a}_1, \mathbf{P}_1\right),
    :label: initial_state

where :math:`\mathbf{a}_1` and :math:`\mathbf{P}_1`  are assumed known. PySSM has a few classes and many functions
that are helpful for inference when analysing the linear Gaussian SSM.

Simulating Data
---------------

PySSM contains functions to simulate data from the SSM in :eq:`Observation_SSM`, :eq:`State_SSM` and 
:eq:`initial_state`. Specifically PySSM facilites the simulation of data through the class :class:`Filter`. As
an example suppose we wished to simulate data from the local linear trend model, where the 
:math:`t^{th}` observation

.. math::

    y_t & =  \mu_t + \varepsilon_t;\,\,\,\varepsilon_t\sim N\left(0,\sigma^2_{\varepsilon}\right), \\
    \mu_{t+1} & =  \mu_{t} + \delta_t + \zeta_t;\,\,\, \zeta_t\sim N\left(0,\sigma^2_{\zeta}\right), \\
    \delta_{t+1} & =  \delta_{t} + \xi_t;\,\,\, \xi_t\sim N\left(0,\sigma^2_{\xi}\right).

Further it is assumed that :math:`\mu_1\sim N(a_1,p_1^2)` and :math:`\delta_1\sim N(a_2,p_2^2)`.
The system matricies for
the SSM are specified by setting

.. math:: \mathbf{Z}_t=1,\,\, \mathbf{R}_t=1,\,\, \mathbf{H}_t=\sigma_{\varepsilon},\,\,
    \mathbf{T}_t=\left[\begin{array}{cc}
    1 & 1\\
    0 & 1\end{array}\right],\,\,\mathbf{G}_t=\mathbf{I}_2,
   
.. math::  \mathbf{Q}_t=\left[\begin{array}{cc}
    \sigma_{\zeta} & 0 \\
    0 & \sigma{\xi}\end{array}\right], \mathbf{a}_1=\left[\begin{array}{c} a_1 \\ a_2\end{array}\right]
    \text{ and } \mathbf{P}=\left[\begin{array}{cc} 
    p_1^2 & 0 \\
    0 & p_2^2 \end{array}\right].

For the simulated data, in the following example, we set :math:`\sigma_{\varepsilon}=20`, :math:`\sigma_{\zeta}=\sigma_{\xi}=0.1`,
:math:`a_1=5`, :math:`a_2=0.1`, :math:`p_1=p_2=0.1`.

.. plot:: pyplots/plot_ll.py
   :include-source:

Computing the likelihood
------------------------

It is straight forward to compute the likelihood for the defined state
space model using PyMCMC. The following code demonstrates how to do it
following on from the class above.

::
    
    print "Log-likelihood = ", filt.log_likelihood()

Where the output is as follows

::
    
    Log-likelihood =  [-4446.27486027]

The System class
--------------------------

The :class:`system` class' most important role is updating and
accessing system matrices.  If using the class :class:`Filter`, as in
the above example, obtaining an instance of the :class:`System` is
done as follows:

::
    
    system = filt.get_system()

where *system* is now a class instance of :class:`System`. Accessing
the system matrices is straightforward as can be seen through the
following examples:

::

    print "System Matrices"
    print "Zt, Rt, Ht, a1 = ", system.zt(), system.rt(), system.ht(), system.a1()
    print "\n p1"
    print system.p1()
    print "\n tt"
    print system.tt()    
    print "\n gt"
    print system.gt()
    print "\n qt"
    print system.qt()

where the output is as follows:

::

    System Matrices
    Zt, Rt, Ht, a1 =  [[ 1.  0.]] [[ 1.]] [[ 400.]] [ 5.   0.1]

     p1
    [[ 0.01  0.  ]
     [ 0.    0.01]]

     tt
    [[ 1.  1.]
     [ 0.  1.]]

     gt
    [[ 1.  0.]
     [ 0.  1.]]

     qt
    [[ 0.01  0.  ]
     [ 0.    0.01]]

**Always update the system matrices using the appropriate functions.**
This is extremely important as certain calculations are computed
during updates that are required by filtering, smoothing and other
algorithms. If updates of system matrices are done using alternative
methods, these will not be computed.

Examples of of updates, following on with the previous example, is
given below:

::

    #Here we update ht directly
    system.update_ht(0.9)
    print "Ht = ", system.ht()

    #Here we update qt directly
    system.update_qt(np.eye(2) * 0.4)
    print "\n Qt"
    print system.qt()

    #Often it is convient when updating one
    #element to make a (shallow) copy of the array
    #as follows
    p1 = system.p1()
    p1[0,0 ] =10.
    system.update_p1(p1)
    print "\n P1"
    print system.p1()

Filtering
---------

Denote
:math:`\mathbf{y}_{s:t}=\left(\mathbf{y}_s^T,\mathbf{y}_{s+1}^T,\dots,\mathbf{y}_n^T\right)^T`,
where :math:`t>s` then the Kalman filter produces
:math:`E\left[\mathbf{x}_{t+1}|\mathbf{y}_{1:t}\right]` and
:math:`V\left[\mathbf{x}_{t+1}|\mathbf{y}_{1:t}\right]`, for
:math:`t=1,2,\dots,n-1.` It is straightforward to compute these
conditional moments using the class :class:`Filter`. Following on from
the above example filtering can be computed as follows,

::

    filt.filter()

We can obtain the filtered estimates for
:math:`E\left[\mathbf{x}_{t+1}|\mathbf{y}_{1:t}\right]` as

::

    filt.astore

and :math:`V\left[\mathbf{x}_{t+1}|\mathbf{y}_{1:t}\right]` as 

::
    
    filt.pstore

Smoothing
---------

Classical smoothing typically refers to finding the
:math:`E\left[\mathbf{x}_{t+1}|\mathbf{y}\right]`, where
:math:`\mathbf{y}=\mathbf{y}_{1:n}.` To obtain the smoothed estimates
in PySSM, following on from the above example, call

::

    filt.smoother()

Note that this function allso calls *filt.filter()*. To obtain the
smoothed estimate, :math:`E\left[\mathbf{x}_{t+1}|\mathbf{y}\right]`
we can simply use

::

    filt.ahat

Simulation Smoothing
--------------------

Bayesian estimation using Markov chain Monte Carlo (MCMC) and
classical estimation based on simulated maximum likelihood often makes
use of what is refered to as simulation smoothing algorithms.  The aim
of simulation smoothing algorithms is to obtain a draw from the
posterior

.. math:: p\left(\mathbf{x}|\mathbf{y},\boldsymbol{\theta}\right),
    :label: state_posterior

where :math:`\boldsymbol{\theta}` are parameters that define the
SSM. Sampling from :eq:`state_posterior` in PySSM is undertaken using
the class :class:`SimSmoother`. To demonstrate the class
:class:`SimSmoother` consider the following MCMC sampling scheme, at
iteration :math:`j` for estimating the parameters in the local linear
trend model:

1. Sample :math:`\mathbf{x}^{(j)}` from
   :math:`p\left(\mathbf{x}|\mathbf{y},\sigma_{\varepsilon}^{(j-1)},
   \sigma_{\zeta}, \sigma_{\xi}\right).`

2. Sample :math:`\sigma_{\varepsilon}^{(j)}` from
   :math:`p\left(\sigma_{\varepsilon}|\mathbf{y},\mathbf{x}^{(j)},\sigma{\zeta}^{(j-1)},
   \sigma_{\xi}^{(j-1)}\right).`

3. Sample :math:`\sigma_{\zeta}^{(j)}` from
   :math:`p\left(\sigma_{\zeta}|\mathbf{y},
   \mathbf{x}^{(j)},\sigma_{\varepsilon}^{(j)},
   \sigma_{\xi}^{(j-1)}\right)`.

4. Sample :math:`\sigma_{\xi}^{(j)}` from
   :math:`p\left(\sigma_{\xi}|\mathbf{y}, \mathbf{x}^{(j)},
   \sigma_{\varepsilon}^{(j)}, \sigma_{\zeta}^{(j)}\right).`

.. plot:: pyplots/lltrend.py
   :include-source:


Further output from the estimation scheme is as follows:

::
    
    The time (seconds) for the MCMC sampler = 28.91
    Number of blocks in MCMC sampler =  3

                        mean          sd         2.5%       97.5%      IFactor
           qt[0]       0.436      0.0629        0.318        0.56         27.6
           qt[1]     0.00741     0.00187      0.00413      0.0112         55.7
              ht       0.325      0.0404        0.245       0.399         24.9

    Acceptance rate  qt  =  1.0
    Acceptance rate  ht  =  1.0

Regressors
----------

If fixed effects are included in the model, then they are incoporated
as optional arguments in the classes :class:`Filter` and
:class:`SimSmoother`. In particular the keyword *xmat* is used for
fixed effects in the measurement equation (refers to
:math:`\mathbf{X}_t` in :eq:`Observation_SSM`) and the keyword *wmat*
is used to add fixed effects to the state equation (refers to
:math:`\mathbf{W}_t` in the :eq:`State_SSM`). When the system matrices
are initialised in either class the optional argument *beta* is used
to pass in an initial value for
:math:`\boldsymbol{\beta}=\left(\boldsymbol{\beta}_m^T,
\boldsymbol{\beta}_s^T\right)^T.`

Consider the following regression model with first order
autoregressive errors,

.. math::
          y_t=\mathbf{X}_t\boldsymbol{\beta}+\varepsilon_t;\,\,\,\varepsilon_t=\phi\varepsilon_{t-1}+\nu_t,

where :math:`|\phi|<1` and :math:`\nu_t\sim N(0,\sigma^2).` This model
can be written in state space form by setting

.. math:: Z_t=1, R_t=0, H_t=1, T_t=\phi, G_t=1 \text{ and },
          Q_t=\sigma^2.

Further if we assume the process has been running since *time
immemorial* then we can assume the initial state is distributed, such
that

.. math:: a_1=0 \text{ and } P_1=\frac{\sigma^2}{1-\phi^2}.

A possible MCMC sampling scheme, at iteration :math:`j` is as follows:

1. Sample :math:`\mathbf{x}^{(j)}` from
   :math:`p\left(\mathbf{x}|\mathbf{y},\boldsymbol{\beta}^{(j-1)},\sigma^{(j-1)},\phi^{(j-1)}\right)`

2. Sample :math:`\boldsymbol{\beta}^{(j)}` from
   :math:`p\left(\boldsymbol{\beta}|\mathbf{y},\mathbf{x}^{(j)},\sigma^{(j-1)}\phi^{(j-1)}\right)`
3. Sample :math:`\sigma^{(j)}` from
   :math:`p\left(\sigma|\mathbf{y},\mathbf{x}^{(j)},\boldsymbol{\beta}^{(j)},\phi^{(j-1)}\right)`

4. Sample :math:`\phi^{(j)}` from
   :math:`p\left(\phi|\mathbf{y},\mathbf{x}^{(j)},\boldsymbol{\beta}^{(j)},\sigma^{(j)}\right)`

In the following example, we simulate code from the autorgressive
model above as well as implement the above MCMC sampling scheme.

Note in the code the regressors should be defined so that
:math:`\text{xmat[:,:,t]}=\mathbf{X}_t` and :math:`\text{wmat[:,:,t]}
= \mathbf{W}_t.` Here *xmat* is a :math:`(p\times k_m \times n)` array
and *wmat* is an :math:`(m\times k_s\times n)` array.
